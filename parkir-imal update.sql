-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2020 at 08:52 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parkir`
--

-- --------------------------------------------------------

--
-- Table structure for table `cekin`
--

CREATE TABLE `cekin` (
  `id_cekin` int(11) NOT NULL,
  `plat_kendaraan` varchar(45) DEFAULT NULL,
  `jenis_kendaraan` varchar(45) DEFAULT NULL,
  `waktu` timestamp(6) NULL DEFAULT NULL,
  `waktu2` bigint(20) DEFAULT NULL,
  `status` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cekin`
--

INSERT INTO `cekin` (`id_cekin`, `plat_kendaraan`, `jenis_kendaraan`, `waktu`, `waktu2`, `status`) VALUES
(1, 'B111CC', 'mobil', '2020-06-30 13:49:54.210000', 1593562220940, 'out'),
(2, 'B112CA', 'mobil', '2020-06-30 13:50:11.679000', 1593563267682, 'out'),
(3, 'B991CA', 'motor', '2020-06-30 13:55:32.087000', 1593566332527, 'out'),
(4, 'B997CC', 'motor', '2020-06-30 13:56:11.974000', 1593577976321, 'out');

-- --------------------------------------------------------

--
-- Table structure for table `tarif`
--

CREATE TABLE `tarif` (
  `id_tarif` int(11) NOT NULL,
  `jenis_kendaraan` enum('motor','mobil') DEFAULT NULL,
  `tarif_1_jam` int(11) DEFAULT NULL,
  `tarif_lebih_dari_1_jam` int(11) DEFAULT NULL,
  `bayar_perhari` int(11) DEFAULT NULL,
  `harian` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tarif`
--

INSERT INTO `tarif` (`id_tarif`, `jenis_kendaraan`, `tarif_1_jam`, `tarif_lebih_dari_1_jam`, `bayar_perhari`, `harian`) VALUES
(1, 'mobil', 2000, 1000, 20000, 50000),
(2, 'motor', 1000, 500, 10000, 25000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role` enum('petugas','admin') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `role`) VALUES
(1, 'mega', 'mega', '$2a$10$zTiwy5jrto2bRu8lD7aXb.Qn33iNQYJHM/WciE10AdDoiw3fUhSRu', 'admin'),
(3, 'admin', 'admin', '$2a$10$fXPoiHqWRBOMrvIg7Niu2OjBK/J44.aGP90qtg1iJBSZow9Ajpi3q', 'admin'),
(4, 'petugas', 'petugas', '$2a$10$bGy0yhpr3jw/TeOoxLFjuubqqkpW7gQml5ci/YYL7OG2uy2qAxz5.', 'petugas'),
(5, 'Imal Zaya', 'imalzy', '$2a$10$sKTm75OBQ5LD7tkhhmr3SON7f37kaW6.oZJqMDn/JJ4V/buZPoUV.', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cekin`
--
ALTER TABLE `cekin`
  ADD PRIMARY KEY (`id_cekin`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id_tarif`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cekin`
--
ALTER TABLE `cekin`
  MODIFY `id_cekin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id_tarif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
