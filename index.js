const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routeUser = require('./routes/User');
const routeTarif = require('./routes/Tarif');
const routeCekin = require('./routes/Cekin');
const routeCekout = require('./routes/Cekout');

app.use(bodyParser.json())
app.use('/', routeUser)
app.use('/', routeTarif)
app.use('/', routeCekin)
app.use('/', routeCekout)

app.listen(3000, () => {
    console.log(`server run on port ${3000}`)
})