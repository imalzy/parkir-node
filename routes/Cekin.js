const router = require('express').Router();
const conn = require('../db/mysql');
var cors = require('cors')

router.use(cors())
router.post('/cekin', async (req,res) => {
    const { plat_kendaraan, jenis_kendaraan, } = req.body
    const date = new Date()
    const data = {
        plat_kendaraan: plat_kendaraan.replace(/[^\w-]+/g,''),
        jenis_kendaraan: jenis_kendaraan,
        waktu: date,
        status: 'in'
    }
    const sql = `INSERT INTO cekin set ? `
    conn.query(sql, data, (err,result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'cekin berhasil ditambahkan',
            status: 200,
            result: data
        })
    })
});

router.get('/cekin', async (req,res) => {
    const sql = `SELECT * FROM cekin`
    conn.query(sql, (err,result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'data tarif',
            status: 200,
            result: result
        })
    })
})

module.exports = router