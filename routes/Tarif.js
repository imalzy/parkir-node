const router = require('express').Router();
const conn = require('../db/mysql');
var cors = require('cors')

router.use(cors())
router.get('/tarif', async (req,res) => {
    const sql = `SELECT * FROM tarif`
    conn.query(sql, (err,result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'data tarif',
            status:200,
            result: result
        })
    })
})

router.get('/tarif/:id', async (req,res) => {
    const { id } = req.params
    const sql = `SELECT * FROM tarif WHERE id_tarif=${id} `
    conn.query(sql, (err,result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'data tarif',
            status:200,
            result: result[0]
        })
    })
})

router.post('/tarif/tambah', async (req,res) => {
    const { jenis_kendaraan, tarif_1_jam, tarif_lebih_dari_1_jam, bayar_perhari, harian } = req.body
    const data = {
        jenis_kendaraan: jenis_kendaraan,
        tarif_1_jam: tarif_1_jam,
        tarif_lebih_dari_1_jam: tarif_lebih_dari_1_jam,
        bayar_perhari: bayar_perhari,
        harian: harian
    }
    const sql = `INSERT INTO tarif set ? `
    conn.query(sql, data, (err,result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'tatif berhasil ditambahkan',
            status:200,
            result: data
        })
    })
})

router.put('/tarif/edit/:id', async (req,res) => {
    const { id } = req.params
    const sql = "UPDATE tarif SET jenis_kendaraan='"+req.body.jenis_kendaraan+"', tarif_1_jam='"+req.body.tarif_1_jam+"', tarif_lebih_dari_1_jam='"+req.body.tarif_lebih_dari_1_jam+"', bayar_perhari='"+req.body.bayar_perhari+"', harian='"+req.body.harian+"' WHERE id_tarif="+id
    conn.query(sql, (err,result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'tarif berhasil di edit',
            status:200,
            result: result
        })
    })
})

router.delete('/tarif/hapus/:id',(req, res) => {
    const { id } = req.params
    let sql = `DELETE FROM tarif WHERE id_tarif=${id}`;
    conn.query(sql, (err, result) => {
        if(err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'user berhasil dihapus',
            status:200,
            result: result
        })
    });
});


module.exports = router