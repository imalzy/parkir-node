const router = require('express').Router();
const conn = require('../db/mysql');
const bcryptjs = require('bcryptjs');
var cors = require('cors')

router.use(cors())
router.get('/user', async (req, res) => {
    const sql = `SELECT * FROM user`
    conn.query(sql, (err, result) => {
        if (err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'data user',
            status: 200,
            result: result
        })
    })
})

router.get('/user/:id', async (req, res) => {
    const { id } = req.params
    const sql = `SELECT * FROM user WHERE id_user=${id} `
    conn.query(sql, (err, result) => {
        if (err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'data user',
            result: result[0]
        })
    })
})

router.post('/auth/login', async (req, res) => {
    const { username, password } = req.body;
    const sql = `SELECT * FROM user WHERE username='${username}' `
    conn.query(sql, async (err, result) => {
        if (err) {
            return res.json({
                message: 'error',
                status: 400,
                result: err,
            })
        } else {
            if (result.length > 0) {
                const comparison = await bcryptjs.compare(password, result[0].password)
                if (comparison) {
                    return res.json({
                        message: 'Login Successfull',
                        status: 200,
                        result: result[0]
                    })
                } else {
                    return res.json({
                        message: 'Username and password does not match',
                        status: 204,
                    })
                }
            } else {
                return res.json({
                    message: 'Username does not exits',
                    status: 206,
                })
            }
        }
    })
})

router.post('/user/tambah', async (req, res) => {
    const { nama, username, password, role } = req.body
    const salt = await bcryptjs.genSalt(10)
    const hashPassword = await bcryptjs.hash(password, salt)
    const data = {
        nama: nama,
        username: username,
        password: hashPassword,
        role: role
    }
    const sql = `INSERT INTO user set ? `
    conn.query(sql, data, (err, result) => {
        if (err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'user berhasil ditambahkan',
            status:200,
            result: data
        })
    })
})

router.put('/user/edit/:id', async (req, res) => {
    const { id } = req.params
    const { nama, username, password, role } = req.body
    const salt = await bcryptjs.genSalt(10)
    const hashPassword = await bcryptjs.hash(req.body.password, salt)
    const sql = "UPDATE user SET nama='" + req.body.nama + "', username='" + req.body.username + "', password='" + hashPassword + "' WHERE id_user=" + req.params.id
    conn.query(sql, (err, result) => {
        if (err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'user berhasil di edit',
            status:200,
            result: result
        })
    })
})

router.delete('/user/hapus/:id', (req, res) => {
    const { id } = req.params
    let sql = `DELETE FROM user WHERE id_user=${id}`;
    let query = conn.query(sql, (err, result) => {
        if (err) {
            return res.json({
                message: 'error',
                result: err
            })
        }
        return res.json({
            message: 'user berhasil dihapus',
            result: result
        })
    });
});


module.exports = router