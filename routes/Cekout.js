const router = require('express').Router();
const conn = require('../db/mysql');
var cors = require('cors')

router.use(cors())
router.post('/cekout', async (req, res) => {
    const { plat_kendaraan } = req.body;
    // const date = new Date();
    // res.json({tgl : date});
    const GetDataKendaraan = "SELECT * FROM cekin WHERE plat_kendaraan='" + plat_kendaraan.replace(/[^\w-]+/g, '') + "'";
    conn.query(GetDataKendaraan, (err, kendaraan) => {
        if (err) {
            return res.json({
                message: 'error',
                result: err
            })
        }

        // const sql = "UPDATE cekin SET status='out', waktu2='" + date + "' WHERE id_cekin=" + kendaraan[0].id_cekin
        const sql = `UPDATE cekin SET status='out', waktu2='${new Date().valueOf()}' WHERE id_cekin=${kendaraan[0].id_cekin}`
        conn.query(sql, (err, result) => { })

        if (kendaraan[0].jenis_kendaraan === 'motor') {
            const getTarif = "SELECT * FROM tarif WHERE jenis_kendaraan='" + kendaraan[0].jenis_kendaraan + "'";
            conn.query(getTarif, (err, tarif) => {
                const getWaktuParkir = new Date().getHours() - new Date(kendaraan[0].waktu).getHours()
                if (getWaktuParkir <= 1) {
                    return res.json({
                        message: 'total pembayaran',
                        totalBayar: tarif[0].tarif_1_jam
                    })
                } else if (getWaktuParkir >= 1) {
                    const total = tarif[0].tarif_1_jam + (tarif[0].tarif_lebih_dari_1_jam * getWaktuParkir)
                    return res.json({
                        message: 'total pembayaran',
                        totalBayar: total
                    })
                } else if (getWaktuParkir === 24) {
                    return res.json({
                        message: 'total pembayaran',
                        totalBayar: tarif[0].bayar_perhari
                    })
                }
            })
        } else if (kendaraan[0].jenis_kendaraan === 'mobil') {
            const getTarif = "SELECT * FROM tarif WHERE jenis_kendaraan='" + kendaraan[0].jenis_kendaraan + "'";
            conn.query(getTarif, (err, tarif) => {
                const getWaktuParkir = new Date().getHours() - new Date(kendaraan[0].waktu).getHours()
                // res.json(getWaktuParkir);
                if (getWaktuParkir <= 1) {
                    return res.json({
                        message: 'total pembayaran',
                        totalBayar: tarif[0].tarif_1_jam
                    })
                } else if (getWaktuParkir >= 1) {
                    const total = tarif[0].tarif_1_jam + (tarif[0].tarif_lebih_dari_1_jam * getWaktuParkir)
                    return res.json({
                        message: 'total pembayaran',
                        totalBayar: total
                    })
                } else if (getWaktuParkir === 24) {
                    return res.json({
                        message: 'total pembayaran',
                        totalBayar: tarif[0].bayar_perhari
                    })
                }
            })
        }
    })
})

module.exports = router